# Numpy generator for tensorflow pipeline

⚠️ 

This is just an upgrade on the image data generator provided within tensorflow.keras,to efficiently load images while applying preprocessing function on the images. However the limitation of the function is that, it only work's for image files (JPEG,PNG etc), This updated version can be useful while you are using higher dimensional numpy vectors saved as .npy files and do not want your cpu to become a bottleneck in the processing.

 This is how you can use this library in your training pipeline


# clone the ripo
!git clone https://gitlab.com/jay.bhatt3/numpy_generator_tensorflow.git

# add libray to your path

import sys  
sys.path.insert(0, '# path of current dir where the ripo is cloned')


# import is as a function 
numpy_generator_tensorflow.image_data_generator import ImageDataGenerator

datagen = ImageDataGenerator()

## use flow_from_dir or dataframe etc based on your requirement
train = datagen.flow_from_dataframe(dataframe = df.iloc[:int(df.shape[0]*.7)],x_col=x_columns,y_col=y_columns,target_size=vec_size,validate_filenames=False,batch_size=batch_size ,shuffle=True)


